//
// Created by egor9814 on 24.11.18.
//

#include <socketspp.hpp>
#include <sys/socket.h>
#include <netinet/in.h>

struct _sockaddr {
    sockaddr_in addr;
    sockaddr* ptr;
    size_t size;

    _sockaddr() : addr {}, ptr(nullptr), size(0) {}

    _sockaddr(const _sockaddr&) = delete;

    _sockaddr(_sockaddr&& s) noexcept : addr(s.addr), ptr(s.ptr), size(s.size) {
        s.ptr = nullptr;
        s.size = 0;
    }

    ~_sockaddr() {
        ptr = nullptr;
        size = 0;
    }
};

static inline _sockaddr operator!(const sockets::detail::SocketAddress& address) {
    _sockaddr a;
    a.addr.sin_addr.s_addr = address.address;
    a.addr.sin_port = address.port;
    a.addr.sin_family = address.family;
    a.ptr = reinterpret_cast<sockaddr*>(&(a.addr));
    a.size = sizeof(a.addr);
    return std::move(a);
}


/// Input

sockets::InputStream::InputStream(int fileDescriptor, const sockets::detail::SocketAddress& address)
    : fd(fileDescriptor), address(address) {}

sockets::InputStream::InputStream(const sockets::InputStream & is)
    : fd(0), address{0, 0, 0} {}

sockets::InputStream::InputStream(sockets::InputStream && is) noexcept : fd(is.fd), address(is.address) {
    is.fd = -1;
    is.address.address = 0;
    is.address.family = 0;
    is.address.port = 0;
}

sockets::InputStream::~InputStream() {
    fd = -1;
    address.address = 0;
    address.family = 0;
    address.port = 0;
}

sockets::InputStream &sockets::InputStream::operator=(const sockets::InputStream & is) {
    if(this != &is) {
        fd = is.fd;
        address = is.address;
    }
    return *this;
}

sockets::InputStream &sockets::InputStream::operator=(sockets::InputStream && is) noexcept {
    if(this != &is) {
        fd = is.fd;
        address = is.address;

        is.fd = -1;
        is.address.address = 0;
        is.address.family = 0;
        is.address.port = 0;
    }
    return *this;
}

long sockets::InputStream::read(void *buffer, unsigned int len) {
    long n = -1;
    try {
        n = recvfrom(fd, buffer, len, 0, nullptr, nullptr);
    } catch (...) {}
    return n;
}


/// Output
sockets::OutputStream::OutputStream(int fileDescriptor, const sockets::detail::SocketAddress& address)
    : fd(fileDescriptor), address(address) {}

sockets::OutputStream::OutputStream(const sockets::OutputStream &)
    : fd(0), address{0, 0, 0} {}

sockets::OutputStream::OutputStream(sockets::OutputStream && os) noexcept : fd(os.fd), address(os.address) {
    os.fd = -1;
    os.address.address = 0;
    os.address.family = 0;
    os.address.port = 0;
}

sockets::OutputStream::~OutputStream() {
    fd = -1;
    address.address = 0;
    address.family = 0;
    address.port = 0;
}

sockets::OutputStream &sockets::OutputStream::operator=(const sockets::OutputStream & os) {
    if (this != &os) {
        fd = os.fd;
        address = os.address;
    }
    return *this;
}

sockets::OutputStream &sockets::OutputStream::operator=(sockets::OutputStream && os) noexcept {
    if (this != &os) {
        fd = os.fd;
        address = os.address;

        os.fd = -1;
        os.address.address = 0;
        os.address.family = 0;
        os.address.port = 0;
    }
    return *this;
}

long sockets::OutputStream::write(void *buffer, unsigned int len) {
    long n = -1;
    try {
        auto addr = !address;
        n = sendto(fd, buffer, len, 0, addr.ptr, static_cast<socklen_t>(addr.size));
    } catch (...) {}
    return n;
}
