#include <iostream>
#include <socketspp.hpp>
#include <memory>
#include <thread>
#include <mutex>
#include <utility>

class User {
    sockets::ClientSocketPtr socket;
    sockets::InputStream is{socket->getInput()};
    sockets::OutputStream os{socket->getOutput()};
    std::string name;

    explicit User(sockets::ClientSocketPtr socket) : socket(std::move(socket)) {
        is >> name;
    }

public:
    static User of(const sockets::ClientSocketPtr &socket) {
        if (socket == nullptr)
            throw sockets::SocketException("cannot accepting client");
        return User(socket);
    }

    ~User() {
        if (socket->isOpen())
            socket->close();
    }

    [[nodiscard]] const std::string& getName() const {
        return name;
    }

    sockets::InputStream& getInput() {
        return is;
    }

    sockets::OutputStream& getOutput() {
        return os;
    }

    void close() {
        socket->close();
    }
};

static std::mutex ioMutex;

/*void userThread(const sockets::ClientSocketPtr& socket) {
    auto u = User::of(socket);
    ioMutex.lock();
    std::cout << "connected user: " << u.getName() << std::endl;
    ioMutex.unlock();

    ioMutex.lock();
    std::cout << "send 'ACCEPT' to " << u.getName() << std::endl;
    ioMutex.unlock();
    u.getOutput() << "ACCEPT";

    std::string fromUser;
    u.getInput() >> fromUser;
    ioMutex.lock();
    std::cout << "message from " << u.getName() << "> " << fromUser << std::endl;
    ioMutex.unlock();
}*/

int main(int argc, char* argv[]) {
    std::cout << "socket server" << std::endl;

    int port = 0;

    for (int i = 1; i < argc; i++) {
        if (argv[i] == std::string("-p")) {
            port = std::stoi(std::string(argv[++i]));
        }
    }

    if (port == 0) {
        std::cout << "enter port> ";
        std::cin >> port;
    }

    //std::thread* clients[2]{nullptr};
    try {
        sockets::ServerSocket server(port);

        /*for (int i = 0; i < 2; i++) {
            clients[i] = new std::thread(::userThread, server.accept());
        }

        for (auto it : clients) {
            if (it->joinable()) {
                it->join();
            }
        }

        for (auto it : clients) {
            delete it;
        }*/

        auto client = server.accept();
        auto in = client->getInput();
        auto out = client->getOutput();

        char buffer[1024]{0};
        while (true) {
            auto size = in.read((void*) buffer, 1023);
            std::stringstream data;
            //while (size > 0) {
                for (long i = 0; i < size; i++) {
                    data << buffer[i];
                }
                //size = in.read((void*) buffer, 1023);
            //}
            auto cmd = data.str();
            size_t len = cmd.size();
            while (len > 0 && (cmd[len-1] == '\r' || cmd[len-1] == '\n'))
                len--;
            cmd = cmd.substr(0, len);
            if (cmd == "exit()") {
                break;
            } else {
                std::cout << "client> " << cmd << std::endl;
            }
        }

        std::cout << "goodbye!" << std::endl;

        return 0;
    } catch (sockets::SocketException& err) {
        std::cerr << std::endl << err.what() << std::endl;
    }
    return -1;
}