macro(find_socketspp current_dir type)
    set(socketspp_dir ${current_dir}/socketspp)
    find_library(SocketsPPLib
            NAMES socketspp
            PATHS ${socketspp_dir}/cmake-build-${type})
    if(SocketsPPLib)
        include_directories(${socketspp_dir}/include)
        message("sockets++ founded!")
    endif(SocketsPPLib)
    #message(${SocketsPPLib})
endmacro(find_socketspp)

macro(use_socketspp module)
    if(SocketsPPLib)
        target_link_libraries(${module} ${SocketsPPLib})
        message("sockets++ linked to ${module}")
    endif(SocketsPPLib)
endmacro(use_socketspp)
