//
// Created by egor9814 on 24.11.18.
//

#ifndef __egor9814__sockets_hpp__
#define __egor9814__sockets_hpp__

#include <exception>
#include <stdexcept>
#include <sstream>
#include <memory>

namespace sockets {

    namespace detail {
        struct SocketAddress {
            unsigned short family;
            unsigned short port;
            unsigned int address;
        };
    }

    class InputStream;
    class OutputStream;


    class ServerSocket;
    class ClientSocket;
    using ClientSocketPtr = std::shared_ptr<ClientSocket>;

    class SocketException;


    class ServerSocket {
        int socket;
        detail::SocketAddress address;
        //unsigned int host;
        std::string host;

        friend class ClientSocket;
    public:
        explicit ServerSocket(unsigned short port, bool nonblock = false);

        ~ServerSocket();

        void close();

        [[nodiscard]] bool isOpen() const ;

        ClientSocketPtr accept(bool nonblock = false);

        [[nodiscard]] const std::string & getHostName() const;

        [[nodiscard]] unsigned int getHostAddress() const;
    };


    class ClientSocket {
        int socket;
        detail::SocketAddress address;
        unsigned int remote_address;
        unsigned short remote_port;

        friend class ServerSocket;

        ClientSocket(int socket, const detail::SocketAddress& address, bool nonblock);

    public:
        ClientSocket(const char host[4], unsigned short port, bool nonblock = false)
            : ClientSocket(
                (((unsigned int)host[0]) << 24u) | (((unsigned int)host[1]) << 16u) |
                        (((unsigned int)host[2]) << 8u) | ((unsigned int)host[3]),
                port,
                nonblock
                ) {}

        ClientSocket(unsigned int host, unsigned short port, bool nonblock = false);

        ~ClientSocket();

        void close();

        [[nodiscard]] bool isOpen() const ;

        [[nodiscard]] InputStream getInput() const ;

        [[nodiscard]] OutputStream getOutput() const ;

        std::string getRemoteAddressName() const;

        const unsigned int& getRemoteAddress() const;

        const unsigned short& getRemotePort() const;
    };


    class InputStream {
        int fd;
        detail::SocketAddress address;

    public:
        InputStream(int fileDescriptor, const detail::SocketAddress& address);

        InputStream(const InputStream&);

        InputStream(InputStream&&) noexcept;

        ~InputStream();

        InputStream&operator=(const InputStream&);

        InputStream&operator=(InputStream&&) noexcept;

        long read(void* buffer, unsigned int len);
    };


    class OutputStream {
        int fd;
        detail::SocketAddress address;

    public:
        explicit OutputStream(int fileDescriptor, const detail::SocketAddress& address);

        OutputStream(const OutputStream&);

        OutputStream(OutputStream&&) noexcept;

        ~OutputStream();

        OutputStream&operator=(const OutputStream&);

        OutputStream&operator=(OutputStream&&) noexcept;

        long write(void* buffer, unsigned int len);
    };


    class SocketException : public std::runtime_error {
    public:
        explicit SocketException(const char* msg) : std::runtime_error(msg) {}

        explicit SocketException(const std::string& msg) : std::runtime_error(msg) {}

        SocketException(const SocketException& se) = default;
    };


    inline InputStream& operator>>(InputStream& is, std::string& out) {
        std::stringstream res;
        char buf[2]{0};
        while (is.read(buf, 1) != 0 && buf[0]) {
            res << buf[0];
        }
        out = res.str();
        return is;
    }

    inline OutputStream& operator<<(OutputStream& os, const std::string& out) {
        os.write((void*) out.c_str(), out.size() + 1);
        return os;
    }

    inline std::string readline(InputStream& is) {
        std::stringstream res;
        char buf[2]{0};
        while (is.read(buf, 1) != 0) {
            if (buf[0] == '\r')
                continue;
            if (buf[0] == '\n' || buf[0] == '\0')
                break;
            res << buf[0];
        }
        return res.str();
    }

    inline void writeline(OutputStream& os, const std::string& s) {
        char buf[2]{0};
        for (auto c : s) {
            buf[0] = c;
            os.write((void *) buf, 1);
        }
        buf[0] = '\n';
        os.write((void *) buf, 1);
    }

}

#endif //__egor9814__sockets_hpp__
