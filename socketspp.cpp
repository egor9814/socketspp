//
// Created by egor9814 on 24.11.18.
//

#include <socketspp.hpp>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <zconf.h>
#include <arpa/inet.h>
#include <netdb.h>

/// Client
sockets::ClientSocket::ClientSocket(int socket, const detail::SocketAddress& address, bool nonblock)
    : socket(socket), address(address) {
    if (nonblock)
        fcntl(socket, F_SETFL, O_NONBLOCK);

    sockaddr_in local {};
    socklen_t local_len = sizeof(local);
    ::getpeername(socket, reinterpret_cast<sockaddr*>(&local), &local_len);
    remote_address = local.sin_addr.s_addr;
    remote_port = local.sin_port;
}

sockets::ClientSocket::ClientSocket(unsigned int host, unsigned short port, bool nonblock)
    : socket(-1), address {AF_INET, port, host}, remote_address(host), remote_port(port) {
    socket = ::socket(AF_INET, SOCK_STREAM, 0);
    if (socket >= 0) {
        sockaddr_in addr {};
        addr.sin_family = address.family;
        addr.sin_port = htons(address.port);
        addr.sin_addr.s_addr = htonl(address.address);
        if (connect(socket, reinterpret_cast<sockaddr*>(&addr), sizeof(addr)) < 0) {
            using namespace std;
            auto b1 = (host >> 24u) & 0xffu;
            auto b2 = (host >> 16u) & 0xffu;
            auto b3 = (host >> 8u) & 0xffu;
            auto b4 = host & 0xffu;
            throw SocketException(
                    "cannot connect to host " +
                    to_string(b1) + '.' + to_string(b2) + '.' + to_string(b3) + '.' + to_string(b4) +
                    ':' + to_string(port)
                    );
        }
        if (nonblock)
            fcntl(socket, F_SETFL, O_NONBLOCK);
    } else {
        throw SocketException("cannot create socket descriptor");
    }
}

sockets::ClientSocket::~ClientSocket() {
    close();
}


void sockets::ClientSocket::close() {
    if (isOpen()) {
        ::close(socket);
        socket = -1;
    }
}

bool sockets::ClientSocket::isOpen() const {
    return socket != -1;
}

sockets::InputStream sockets::ClientSocket::getInput() const {
    if (!isOpen())
        throw SocketException("socket not opened");
    return sockets::InputStream(socket, address);
}

sockets::OutputStream sockets::ClientSocket::getOutput() const {
    if (!isOpen())
        throw SocketException("socket not opened");
    return sockets::OutputStream(socket, address);
}

std::string sockets::ClientSocket::getRemoteAddressName() const {
    std::stringstream ss;
    for (int i = 0u; i < 4; i++) {
        if (i != 0)
            ss << '.';
        ss << ((remote_address >> (i * 8u)) & 0xffu);
    }
    return ss.str();
}

const unsigned int &sockets::ClientSocket::getRemoteAddress() const {
    return remote_address;
}

const unsigned short &sockets::ClientSocket::getRemotePort() const {
    return remote_port;
}




/// Server
sockets::ServerSocket::ServerSocket(unsigned short port, bool nonblock)
    : socket(-1), address {AF_INET, port, INADDR_ANY} {
    socket = ::socket(AF_INET, SOCK_STREAM, 0);
    if (socket >= 0) {
        sockaddr_in addr {};
        if (nonblock)
            fcntl(socket, F_SETFL, O_NONBLOCK);
        addr.sin_family = address.family;
        addr.sin_port = htons(address.port);
        addr.sin_addr.s_addr = address.address;

        if (bind(socket, reinterpret_cast<sockaddr*>(&addr), sizeof(addr)) < 0) {
            throw SocketException(
                    "cannot bind server on localhost with port " + std::to_string(port)
                    );
        }

        listen(socket, INT32_MAX-1);

        char strbuf[1024];
        if (gethostname(strbuf, sizeof(strbuf)) == 0) {
            host = strbuf;
        }
    } else {
        throw SocketException(
                "cannot create server on localhost with port " + std::to_string(port)
        );
    }
}

sockets::ServerSocket::~ServerSocket() {
    close();
}

void sockets::ServerSocket::close() {
    if (isOpen()) {
        ::close(socket);
        socket  = -1;
        //host = 0;
        host.clear();
    }
}

bool sockets::ServerSocket::isOpen() const {
    return socket != -1;
}

sockets::ClientSocketPtr sockets::ServerSocket::accept(bool nonblock) {
    sockaddr_in addr {};
    socklen_t addr_len = sizeof(addr);
    auto client = ::accept(socket,
            reinterpret_cast<sockaddr*>(&addr), &addr_len);
    if (client < 0)
        return nullptr;
    return ClientSocketPtr(new ClientSocket(client, address, nonblock));
}

const std::string & sockets::ServerSocket::getHostName() const {
    /*std::stringstream ss;
    for (int i = 0u; i < 4; i++) {
        if (i != 0)
            ss << '.';
        ss << ((host >> (i * 8u)) & 0xffu);
    }
    return ss.str();*/
    return host;
}

unsigned int sockets::ServerSocket::getHostAddress() const {
    auto host = gethostbyname(this->host.c_str());
    if (!host)
        return 0;
    auto addr = reinterpret_cast<in_addr*>(host->h_addr);
    return addr->s_addr;
}
