//
// Created by egor9814 on 24.11.18.
//

#include <iostream>
#include <socketspp.hpp>
#include <cstring>

int main(int argc, char* argv[]) {
    std::cout << "Hello, World!" << std::endl;

    int port = 0;
    std::string name;

    for (int i = 1; i < argc; i++) {
        if (argv[i] == std::string("-p")) {
            port = std::stoi(std::string(argv[++i]));
        }
        if (argv[i] == std::string("-n")) {
            name = argv[++i];
        }
    }

    if (port == 0) {
        std::cout << "enter port> ";
        std::cin >> port;
    }

    while (name.empty()) {
        std::cout << "enter name> ";
        std::cin >> name;
    }

    try {
        sockets::ClientSocket client(0x7f000001u, static_cast<unsigned short>(port));
        auto in = client.getInput();
        auto out = client.getOutput();

        out << name;

        std::string fromServer;
        in >> fromServer;
        if (fromServer != "ACCEPT") {
            client.close();
            throw sockets::SocketException("invalid message from server: " + fromServer);
        }
        std::cout << "message from server> " << fromServer << std::endl;

        std::cout << "send 'HELLO' to server" << std::endl;
        out << "HELLO";

        std::cout << "goodbye!" << std::endl;

        client.close();
        return 0;
    } catch (sockets::SocketException& err) {
        std::cerr << std::endl << err.what() << std::endl;
    }
    return -1;
}