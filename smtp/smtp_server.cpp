//
// Created by egor9814 on 9/16/19.
//

#include <socketspp.hpp>
#include <iostream>
#include <vector>
#include <fstream>
#include <map>
#include <algorithm>

using namespace sockets;

enum class State {
    Default,
    MailFrom,
    RcptTo
};

std::string strip(const std::string& s) {
    size_t begin = 0, end = s.length();
    while (begin < s.length()) {
        switch (s[begin]) {
            case ' ':
            case '\t':
                begin++;
                continue;
        }
        break;
    }
    while (end > 0) {
        switch (s[end]) {
            case ' ':
            case '\t':
                end--;
                continue;
        }
        break;
    }
    if (begin == 0 && end == s.length())
        return s;
    else
        return s.substr(begin, end - begin);
}

std::vector<std::string> split(const std::string& s) {
    int state = 0;
    std::string buf;
    std::vector<std::string> res;
    for (char c : s) {
        if (c == ' ' || c == '\t') {
            if (state == 0) {
                if (!buf.empty()) {
                    res.push_back(buf);
                    buf.clear();
                }
                state = 1;
            }
        } else {
            state = 0;
            buf += c;
        }
    }
    if (!buf.empty()) {
        res.push_back(buf);
        buf.clear();
    }
    return std::move(res);
}

using fptr = std::shared_ptr<std::ofstream>;
using fptr_list = std::map<std::string, fptr>;

std::string toLower(const std::string& str) {
    std::string res(str);
    std::for_each(std::begin(res), std::end(res), [](char& c) {
        c = static_cast<char>(tolower(c));
    });
    return res;
}

int main(int argc, char* argv[]) {
    int port = 25;

    for (int i = 1; i < argc; i++) {
        if (argv[i] == std::string("-p")) {
            port = std::stoi(std::string(argv[++i]));
        }
    }

    std::cout << "starting SMTP server on port " << port << std::endl;

    fptr_list files;
    try {
        ServerSocket server(static_cast<unsigned short>(port));
        std::cout << "server> start successful" << std::endl;
        std::cout << "server> server address " << server.getHostName() << ':' << port << std::endl;
        std::cout << "server> wait client..." << std::endl;

        auto run = true;
        while (run) {
            auto client = server.accept();
            std::cout << "server> client connected: " << client->getRemoteAddressName();
            std::cout << ':' << client->getRemotePort() << std::endl;

            auto in = client->getInput();
            auto out = client->getOutput();

            writeline(out, "220 " + server.getHostName() + " SMTP server");

            auto state = State::Default;
            auto runClient = true;
            while (runClient) {
                auto line = strip(readline(in));
                std::cout << "client> " << line << std::endl;

                auto command = split(line);
                auto cmd = toLower(command[0]);

                if (cmd == "quit") {
                    runClient = false;
                    writeline(out, "221 channel closed");
                    client->close();
                    if (command.size() >= 2 && command[1] == "server") {
                        run = false;
                    }
                    break;
                } else if (cmd == "helo") {
                    std::stringstream sout;
                    sout << "250 " << server.getHostName();
                    writeline(out, sout.str());
                } else if (cmd == "mail") {
                    state = State::MailFrom;
                    auto error = false;
                    std::string name;
                    if (command.size() == 1) {
                        error = true;
                    } else {
                        if (toLower(command[1]) == "from:") {
                            if (command.size() >= 3)
                                name = strip(command[2]);
                        } else if (command.size() >= 3 && command[2] == ":" && toLower(command[1]) == "from") {
                            if (command.size() >= 4)
                                name = strip(command[3]);
                        } else {
                            error = true;
                        }
                    }
                    if (error) {
                        writeline(out, "500 syntax error, usage> MAIL From: <NAME>");
                    } else {
                        std::cout << "server> mail from '" << name << "'" << std::endl;
                        writeline(out, "250 ok");
                    }
                } else if (cmd == "rcpt") {
                    if (state != State::MailFrom && state != State::RcptTo) {
                        writeline(out, "503 expected MAIL From or RCPT To");
                    } else {
                        state = State::RcptTo;
                        auto error = false;
                        std::string name;
                        if (command.size() == 1) {
                            error = true;
                        } else {
                            if (toLower(command[1]) == "to:") {
                                if (command.size() >= 3)
                                    name = strip(command[2]);
                            } else if (command.size() >= 3 && command[2] == ":" && toLower(command[1]) == "to") {
                                if (command.size() >= 4)
                                    name = strip(command[3]);
                            } else {
                                error = true;
                            }
                        }
                        if (error) {
                            writeline(out, "500 syntax error, usage> RCPT To: <NAME>");
                        } else {
                            if (!files.count(name)) {
                                std::cout << "rcpt to '" << name << "'" << std::endl;
                                files[name] = std::make_shared<std::ofstream>(name, std::ios_base::app);
                                writeline(out, "250 ok");
                            } else {
                                writeline(out, "550 recipient is already attached");
                            }
                        }
                    }
                } else if (cmd == "data") {
                    if (state == State::RcptTo) {
                        state = State::Default;
                        writeline(out, "354 intermediate");
                        auto msg = strip(readline(in));
                        while (msg != ".") {
                            std::cout << "server> DATA: " << msg << std::endl;
                            for (auto &it : files) {
                                *(it.second) << msg << std::endl;
                            }
                            msg = strip(readline(in));
                        }
                        writeline(out, "250 ok");
                        std::cout << "server> end of message" << std::endl;
                        for (auto &it : files) {
                            it.second->close();
                        }
                        files.clear();
                    } else {
                        writeline(out, "503 expected RCPT to");
                    }
                } else {
                    writeline(out, "500 syntax error or unknown command");
                }
            }

            std::cout << "server> client detached" << std::endl;
            if (run)
                std::cout << "server> wait new client..." << std::endl;
        }

        return 0;
    } catch (const SocketException& error) {
        std::cerr << error.what() << std::endl;
        if (!files.empty()) {
            for (auto &it : files) {
                it.second->close();
            }
            files.clear();
        }
    }
    return 1;
}
